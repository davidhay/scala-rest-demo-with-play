# Rest Demo with Scala, Play and Slick #

This Scala/Play Demo creates a Rest API for Users/Tasks. This app uses Scala Play  with H2 in memory database.
The rest endpoints have integration tests written for them using the play *FakeApplication* and *FakeRequests* together with the
in memory database.

## REST URLS FOR READ/GET ##

*  http://localhost:9000/users 
*  http://localhost:9000/users/{userid} 
*  http://localhost:9000/users/{userid}/tasks 
*  http://localhost:9000/users/{userid}/tasks/{taskid}
 
## REST URLS POST/CREATE ##
to create a new user

post to http://localhost:9000/users

```
#!python
curl -i -X POST -H "Content-Type: application/json" \
-d '{"firstname" : "santa","lastname" : "Saint", "username" : "Nicholas"}' \
http://localhost:9000/users
```

```
#!python
HTTP/1.1 201 Created
Location: http://localhost:9000/users/3
Content-Length: 0
```


to create a new user task

post to http://localhost:9000/users/1/tasks

```
#!python
curl -i -X POST -H "Content-Type: application/json" \
-d '{"description":"desc","completed":false,"dueDate":"2015-12-31T23:00:00","priority":"HI"}' \
http://localhost:9000/users/1/tasks
```

```
#!python
HTTP/1.1 201 Created
Location: http://localhost:9000/users/1/tasks/24
Content-Length: 0
```