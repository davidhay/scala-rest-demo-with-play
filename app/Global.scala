import play.api._
import play.api.mvc.{Handler, RequestHeader, Action, Controller}

object TrailingSlashRedirector extends Controller {
  def redirect(request: RequestHeader) = Action {
    val target = request.path.dropRight(1)
    Logger.info(s"The route [${request.path}] ends in slash. redirecting to ${target}")
    Redirect(target, request.queryString, 301)
  }
}

object Global extends GlobalSettings {

  override def onRouteRequest(request: RequestHeader): Option[Handler] = {

    val EndsWithASlash= """/(.*)/$""".r

    request.path match {  
      case EndsWithASlash(_) => {
       Some(TrailingSlashRedirector.redirect(request)) 
      }
      case _ => super.onRouteRequest(request)
    }

  }  

}