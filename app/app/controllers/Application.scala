package app.controllers

import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action { implicit request =>
    val session = request.session
    val  counterVal:Option[String] = session.get("counter")
    val  counter = counterVal.map { str => str.toInt }.getOrElse(0)
    val  newCounter = counter + 1
    val now = new java.util.Date()
    Ok(views.html.index(now)).withSession(session + ("counter" -> newCounter.toString))
  }

}