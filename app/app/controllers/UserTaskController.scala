package app.controllers

import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api._
import app.service.UserService
import app.models.User
import app.models.Task
import app.models.Priority
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.JsValue
import play.api.libs.json.Writes.dateWrites // do not import everything here, especially DefaultDateWrites
object UserTaskController extends Controller {

  import play.api.db.slick.Config.driver.simple._
  val service: UserService = new app.service.DbUserService

  import app.util.Utils._

  def getUsers() = DBAction { implicit request =>
    val users = service.getUsers();
    val jsonUsers: List[JsValue] = users map { u => Json.toJson(u) }
    Ok(Json.toJson(jsonUsers))
  }

  def getUser(userId: Long) = DBAction { implicit request =>
    val user: Option[User] = service.getUser(userId);
    val userJson: Option[JsValue] = user map { u => Json.toJson(u) }
    userJson.map(Ok(_)).getOrElse(NotFound)
  }

  def getUserTasks(userId: Long) = DBAction { implicit request =>
    //get the tasks for the user - may be None
    val userTasksO: Option[List[Task]] = service.getUserTasks(userId);

    //convert Option[List[Task]] => Option[List[JsValue]]
    val jsonTasksO: Option[List[JsValue]] = userTasksO map { ut => { ut map { t => Json.toJson(t) } } }

    //convert Option[List[JsValue]] => Option[JsValue]
    val jsonO: Option[JsValue] = jsonTasksO map (Json.toJson(_))

    //if defined return OK otherwise NotFound
    jsonO.map(Ok(_)).getOrElse(NotFound)
  }

  def getUserTask(userId: Long, taskId: Long) = DBAction { implicit request =>
    val task: Option[Task] = service.getUserTask(userId, taskId);
    val taskJsonO = task map { t => Json.toJson(t) }
    //if defined return OK otherwise NotFound
    taskJsonO.map(Ok(_)).getOrElse(NotFound)
  }

  def saveUser = DBAction(BodyParsers.parse.json) { implicit request =>
    val userJsResult = request.body.validate[User]
    userJsResult.fold (
      errors => {
        BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toFlatJson(errors)))
      },
      user => {
        service.saveUser(user) map { saved =>
          val part1:Call = routes.UserTaskController.getUser(saved.id.get);
          println(s"part1 $part1")
          val location = part1.absoluteURL(false)
          println(s"Generated Location IS $location")
          Created.withHeaders(LOCATION -> location);
        } getOrElse (BadRequest)
      })
  }
  
  def saveTask(userId:Long) = DBAction(BodyParsers.parse.json) { implicit request =>
    val userO: Option[User] = service.getUser(userId);
    
    userO.map { user => 
    
    val taskJsResult = request.body.validate[Task]
    taskJsResult.fold (
      errors => {
        BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toFlatJson(errors)))
      },
      task => {
        val toSave = task.copy(userId = userId)
        service.saveTask(toSave) map { saved =>
          val part1:Call = routes.UserTaskController.getUserTask(userId,saved.id.get);
          println(s"part1 $part1")
          val location = part1.absoluteURL(false)
          println(s"Generated Location IS $location")
          Created.withHeaders(LOCATION -> location);
        } getOrElse (BadRequest)
      })
      
      } getOrElse (NotFound(s"The user with id ${userId} cannot be found"))
  }
}