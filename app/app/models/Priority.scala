package app.models

object Priority {
  def valueOf(v:String):Priority = {
    v match {
      case MED.name => MED
      case HI.name => HI;
      case _ => LO;
    }
  }
}

sealed trait Priority {
  def name:String
}
case object MED extends Priority { val name = "MED"}
case object LO extends Priority { val name = "LO"}
case object HI extends Priority {val name = "HI"}
