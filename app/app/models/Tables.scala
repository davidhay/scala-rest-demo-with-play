package app.models

import play.api.db.slick.Config.driver.simple._
import java.util.Date
case class User(id: Option[Long] = None, username: String, firstname: String, lastname: String)

case class Task(
  id: Option[Long],
  dueDate: Date,
  description: String,
  priority: Priority,
  completed: Boolean,
  userId: Long = 0)

object Tables {
  implicit val util2sqlDateMapper = MappedColumnType.base[java.util.Date, java.sql.Date](
    { utilDate => new java.sql.Date(utilDate.getTime()) },
    { sqlDate => new java.util.Date(sqlDate.getTime()) })

  implicit val priority2StringMapper = MappedColumnType.base[Priority, String](
    { priority => priority.name },
    { name => Priority.valueOf(name) })

  val userTable = TableQuery[UserTable]

  class UserTable(tag: Tag) extends Table[User](tag, "person") {
    def id: Column[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def username: Column[String] = column[String]("username", O.NotNull)
    def first: Column[String] = column[String]("first", O.NotNull)
    def last: Column[String] = column[String]("last", O.NotNull)
    def * = (id.?, username, first, last) <> (User.tupled, User.unapply)
  }

  class TaskTable(tag: Tag) extends Table[Task](tag, "person_task") {

    def id: Column[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def dueDate: Column[Date] = column[Date]("due_date", O.NotNull)
    def userId: Column[Long] = column[Long]("user_id", O.NotNull)
    def completed: Column[Boolean] = column[Boolean]("completed", O.NotNull)
    def priority: Column[Priority] = column[Priority]("priority", O.NotNull)
    def description: Column[String] = column[String]("description", O.NotNull)
    def user = foreignKey("user_fk", userId, userTable)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)
    def * = (id.?, dueDate, description, priority, completed, userId) <> (Task.tupled, Task.unapply)
  }

  val taskTable = TableQuery[TaskTable]
}