package app.repo

/**
 * @author davidhay
 */
import play.api.db.slick.Config.driver.simple._
import app.models.Task
class SlickTaskRepository extends TaskRepository {

  import app.models.Tables.taskTable

  def getTaskList()(implicit s: Session): List[Task] = { taskTable.list }

  def getTaskList(userId:Long)(implicit s: Session): List[Task] = { 
    taskTable.filter{ _.userId === userId }.list 
  }
  
  def insert(task:Task)(implicit s: Session): Long = {
      taskTable.returning(taskTable.map(_.id)).insert(task)
  }

  def update(task: Task)(implicit s: Session): Long = {
      taskTable.filter { _.id === task.id }.update(task)
  }

  def delete(id: Long)(implicit s: Session): Long = {
      taskTable.filter { _.id === id }.delete
  }

}




