package app.repo

/**
 * @author davidhay
 */
import play.api.db.slick.Config.driver.simple._
import app.models.User
class SlickUserRepository extends UserRepository  {

  import app.models.Tables.userTable
  
  def getUserList()(implicit s: Session): List[User] = {
    userTable.list
  }
  
  def getUser(userId:Long)(implicit s: Session):Option[User] = {
      userTable.list.find(u=>u.id.get == userId)
  }
  
  def insert(user: User)(implicit s: Session): Long = {
      userTable.returning(userTable.map(_.id)).insert(user)
  }

  def update(user: User)(implicit s: Session): Long = {
      userTable.filter { _.id === user.id }.update(user)
  }

  def delete(id: Long)(implicit s: Session): Long = {
      userTable.filter { _.id === id }.delete
  }

}
