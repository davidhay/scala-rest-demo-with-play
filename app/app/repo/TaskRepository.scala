package app.repo

import app.models._

/**
 * @author davidhay
 */
import play.api.db.slick.Config.driver.simple._
trait TaskRepository {

  def getTaskList()(implicit s: Session): List[Task];

  def getTaskList(userId: Long)(implicit s: Session): List[Task]

  def insert(task: Task)(implicit s: Session): Long

  def update(task: Task)(implicit s: Session): Long

  def delete(taskId: Long)(implicit s: Session): Long

}


