package app.repo

import app.models._

/**
 * @author davidhay
 */
import play.api.db.slick.Config.driver.simple._
trait UserRepository  {

  def getUserList()(implicit s: Session): List[User]
  
  def insert(user: User)(implicit s: Session): Long 

  def update(user: User)(implicit s: Session): Long 

  def delete(id: Long)(implicit s: Session): Long

  def getUser(userId:Long)(implicit s: Session):Option[User]
  
}


