package app.service

import app.repo.UserRepository
import app.repo.TaskRepository
import app.models.Task
import app.models.User

class DbUserService extends UserService {

  import play.api.db.slick.Config.driver.simple._

  def  userRepo:UserRepository = new app.repo.SlickUserRepository()
  def  taskRepo:TaskRepository = new app.repo.SlickTaskRepository()
  
  def getUsers()(implicit s: Session):List[User] = {
    userRepo.getUserList();    
  }
  
  def getUser(userId:Long)(implicit s: Session):Option[User]= {
    userRepo.getUser(userId)
  }
 
  def getUserTasks(userId:Long)(implicit s: Session):Option[List[Task]] = {
     userRepo.getUser(userId).map { user => taskRepo.getTaskList(user.id.get) }
  }
  
  def getUserTask(userId:Long,taskId:Long)(implicit s: Session):Option[Task]={
     val taskListO:Option[List[Task]] = userRepo.getUser(userId).map { user => taskRepo.getTaskList(user.id.get) }
     val taskO:Option[Task] = taskListO flatMap {taskList => taskList find { t => t.id.get == taskId }}
     taskO
  }
  
  def saveUser(user:User)(implicit s: Session):Option[User]={
    val savedId = userRepo.insert(user)
    Some(user.copy(id=Some(savedId)))
  }
  
  
  def saveTask(task:Task)(implicit s: Session):Option[Task]={
    val savedId = taskRepo.insert(task)
    Some(task.copy(id=Some(savedId)))
  }


}

