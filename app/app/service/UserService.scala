package app.service

import app.models.{User,Task}
import play.api.db.slick.Config.driver.simple.Session
trait UserService {

  def getUsers()(implicit s: Session):List[User];
  
  def getUser(userId:Long)(implicit s: Session):Option[User]
 
  def getUserTasks(userId:Long)(implicit s: Session):Option[List[Task]]
  
  def getUserTask(userId:Long,taskId:Long)(implicit s: Session):Option[Task]
  
  def saveUser(user:User)(implicit s: Session):Option[User]
  
  def saveTask(task:Task)(implicit s: Session):Option[Task]
}