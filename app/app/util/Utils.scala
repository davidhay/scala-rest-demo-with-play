package app.util

import java.text.SimpleDateFormat
import java.util.Date
import app.models.Priority
import app.models.Task
import app.models.User
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.Reads
import play.api.libs.json.Writes
import play.api.libs.json.Writes.dateWrites
import play.api.libs.json.JsResult
import play.api.libs.json.JsSuccess

object Utils {
  
  class DateString(val s: String) { 
    def toDate : Date = new SimpleDateFormat("dd/MM/yyyy").parse(s) 
  }

  implicit def str2datestr(s: String) : DateString = new DateString(s)
  
  implicit val myDateWrites = dateWrites("yyyy-MM-dd'T'HH:mm:ss'Z'")
  
  implicit val priorityWrites = new Writes[Priority]{
    def writes(p:Priority):JsValue = JsString(p.name)
  }
  implicit val priorityReads = new Reads[Priority]{
    def reads(json:JsValue):JsResult[Priority] = {
       val p = Priority.valueOf(json.as[String])
       new JsSuccess(p)
    }
  }
  implicit val userReads = new Reads[User]{
    def reads(json: JsValue): JsResult[User] = {
        val username = (json \ "username").as[String]
        val firstname = (json \ "firstname").as[String]
        val lastname = (json \ "lastname").as[String]
        val user = User(username=username,firstname=firstname,lastname=lastname) 
        new JsSuccess(user)
    }
  }
  implicit val taskReads = new Reads[Task]{
    def reads(json: JsValue): JsResult[Task] = {
        val completed = (json \ "completed").as[Boolean]
        val description = (json \ "description").as[String]
        val dueDate = (json \ "dueDate").as[Date]
        val priority = (json \ "priority").as[Priority]
        val task = Task(id=None,completed=completed,description=description,dueDate=dueDate,priority=priority) 
        new JsSuccess(task)
    }
  }
  implicit val userWrites = Json.writes[User]
  implicit val taskWrites = Json.writes[Task]
  
}