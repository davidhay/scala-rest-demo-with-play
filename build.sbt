name := """play-demo"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.typesafe.play" %% "play-slick" % "0.8.1"
)

libraryDependencies += "org.scalatestplus" %% "play" % "1.2.0" % "test"

libraryDependencies += "org.webjars" % "webjars-play_2.11" % "2.3.0-3"

libraryDependencies += "org.webjars" % "bootstrap" % "3.3.4"

libraryDependencies += "org.webjars" % "jquery" % "2.1.3"

