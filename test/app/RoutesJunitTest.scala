package app

import org.junit.Test
import org.scalatest._
import play.api.test._
import play.api.test.Helpers._
import org.scalatestplus.play._
import app.util.Utils._
import scala.concurrent.Future
import play.api.mvc.Result
import play.api.libs.json._
import app.models.Priority

class RoutesJunitTest extends PlaySpec with OneAppPerSuite {

  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = Map("ehcacheplugin" -> "disabled","application.hostname"->"localhost","application.port"->"8080"))

  @Test
  def testGetUsers() {
    running(app) {
      val resultO: Option[Future[Result]] = route(FakeRequest(GET, "/users"));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(200)
      contentType(result).get must be("application/json")

      val json = contentAsJson(result)

      (json(0) \ "username").as[String] must be("spiderman")
      (json(0) \ "firstname").as[String] must be("Peter")
      (json(0) \ "lastname").as[String] must equal("Parker")

      (json(1) \ "username").as[String] must be("batman")
      (json(1) \ "firstname").as[String] must be("Bruce")
      (json(1) \ "lastname").as[String] must be("Wayne")

    }
  }

  @Test
  def testGetUserForSpiderman() {
    running(app) {
      val resultO: Option[Future[Result]] = route(FakeRequest(GET, "/users/1"));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(200)
      contentType(result).get must be("application/json")
      val json = contentAsJson(result)
      checkUser1(json)
    }
  }
  @Test
  def testGetUserForBatman() {
    running(app) {
      val resultO: Option[Future[Result]] = route(FakeRequest(GET, "/users/2"));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(200)
      contentType(result).get must be("application/json")
      val json = contentAsJson(result)
      checkUser2(json)
    }
  }

  @Test
  def testGetUserTasksForSpiderman() {
    running(app) {
      val resultO: Option[Future[Result]] = route(FakeRequest(GET, "/users/1/tasks"));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(200)
      contentType(result).get must be("application/json")

      val json = contentAsJson(result)

      checkTask11(json(0))
      checkTask12(json(1))
      checkTask13(json(2))
    }
  }

  @Test
  def testGetUserTask11ForSpiderman() {
    running(app) {
      val resultO: Option[Future[Result]] = route(FakeRequest(GET, "/users/1/tasks/11"));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(200)
      contentType(result).get must be("application/json")

      val json = contentAsJson(result)

      checkTask11(json)
    }
  }

  @Test
  def badRoute() {
    running(app) {
      val resultO = route(FakeRequest(GET, "/bad"));
      resultO must be('empty)
    }
  }

  def checkUser1(json: JsValue): Unit = {
    (json \ "username").as[String] must be("spiderman")
    (json \ "firstname").as[String] must be("Peter")
    (json \ "lastname").as[String] must equal("Parker")
  }
  def checkUser2(json: JsValue): Unit = {
    (json \ "username").as[String] must be("batman")
    (json \ "firstname").as[String] must be("Bruce")
    (json \ "lastname").as[String] must be("Wayne")
  }
  def checkTask11(json: JsValue): Unit = {
    (json \ "description").as[String] must be("task 11")
    (json \ "priority").as[String] must be("LO")
    (json \ "dueDate").as[String] must equal("2016-01-11T00:00:00Z")
    (json \ "completed").as[Boolean] must equal(false)
  }
  def checkTask12(json: JsValue): Unit = {
    (json \ "description").as[String] must be("task 12")
    (json \ "priority").as[String] must be("MED")
    (json \ "dueDate").as[String] must equal("2016-01-12T00:00:00Z")
    (json \ "completed").as[Boolean] must equal(true)
  }

  def checkTask13(json: JsValue): Unit = {
    (json \ "description").as[String] must be("task 13")
    (json \ "priority").as[String] must be("HI")
    (json \ "dueDate").as[String] must equal("2016-01-13T00:00:00Z")
    (json \ "completed").as[Boolean] must equal(false)
  }
  
  @Test
  def testCreateUser():Unit = {
    running(app) {
    val json = Json.obj(
            "username" -> JsString("newuser"),
            "firstname" -> JsString("new"),
            "lastname" -> JsString("user")
                 )
      val resultO: Option[Future[Result]] = route(new FakeRequest(method=POST, uri="/users", body=json, headers=FakeHeaders(
                              Seq("Content-type"->Seq("application/json"), "Host" -> Seq("localhost:1234"))
      )));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(201)
      println(s"The result is $result")
      header(LOCATION,result) must be (Some("http://localhost:1234/users/3"))
    }
  }

  @Test
  def testCreateTask():Unit = {
    running(app) {
    val json = Json.obj(
            "completed" -> JsBoolean(true),
            "dueDate" -> JsString("2016-01-12T00:00:00Z"),
            "priority" -> JsString("MED"),
            "description" -> JsString("A new Task to save")
                 )
      val resultO: Option[Future[Result]] = route(new FakeRequest(method=POST, uri="/users/1/tasks", body=json, headers=FakeHeaders(
                              Seq("Content-type"->Seq("application/json"), "Host" -> Seq("localhost:1234"))
      )));
      resultO must be('defined)
      val result = resultO.get
      status(result) must be(201)
      println(s"The result is $result")
      header(LOCATION,result) must be (Some("http://localhost:1234/users/1/tasks/24"))
    }
  }

}